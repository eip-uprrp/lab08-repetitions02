#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QShortcut>
#include <QImage>
#include <QDebug>
#include <cmath>

/// Main class for the program.
///
/// This class contains the main functions for the program.
/// There are functions for merging two images together,
/// a function that creates a ghost version of the first image on the merge image,
/// and the functions for the GUI.

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void MergeImages(const QImage &, const QImage &, QImage &, int, bool=false, int=0, int=0, int=0, int=0) ;
    void Ghost(const QImage &, const QImage &, QImage & , int, int, int, int=0, int=0) ;
    ~MainWindow();

private slots:
    void on_ghostBox_clicked();
    void on_btnSelectImage_clicked();
    void on_btnSelectBackground_clicked();
    void on_btnMergeImages_clicked();
    void on_btnSaveImage_clicked();
    // For clickable label
    void Mouse_Pressed();
    void Mouse_PressedBackground() ;
    void on_thresholdSlider_actionTriggered();

private:
    Ui::MainWindow *ui;
    QImage originalImage;
    QImage backgroundImage;
    QImage mergedImage;
};

#endif // MAINWINDOW_H
