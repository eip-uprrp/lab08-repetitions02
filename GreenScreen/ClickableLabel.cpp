#include "ClickableLabel.h"

///
/// Functions that make a label clickable.
///
ClickableLabel::ClickableLabel(QWidget *parent) : QLabel(parent) {
    this->x = 0;
    this->y = 0;
}

void ClickableLabel::mouseMoveEvent(QMouseEvent *ev){
    emit Mouse_Pos();
    this->x = ev->x();
    this->y = ev->y();
}

void ClickableLabel::mousePressEvent(QMouseEvent *ev){
    x = ev->x();
    y = ev->y();
    emit Mouse_Pressed();
}

void ClickableLabel::leaveEvent(QEvent *){
    emit Mouse_Left();
}
