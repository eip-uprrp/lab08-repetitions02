#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QMouseEvent>
#include <QDebug>

/// A class that permits a label to be clicked.
///
/// With this class we can determine de mouse position when is pressed
/// and when the mouse left the label.
///
class ClickableLabel : public QLabel{
    Q_OBJECT
public:
    explicit ClickableLabel(QWidget *parent = 0);
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void leaveEvent(QEvent *);
    int x,y;

signals:
    void Mouse_Pressed();
    void Mouse_Pos();
    void Mouse_Left();
};

#endif // CLICKABLELABEL_H
