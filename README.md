#Lab. 8: Estructuras de repetición II

Una de las ventajas de utilizar programas de computadoras es que podemos realizar tareas repetitivas facilmente. Los ciclos como `for`, `while`, y `do-while` son un tipo de estructura de control que nos permiten repetir un conjunto de instrucciones. A estas estructuras también les llamamos *estructuras de repetición*. 


#Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes utilizarán técnicas de "pantalla verde" para practicar el uso de ciclos anidados en la manipulación de arreglos bi-dimensionales. También practicarán el uso de expresiones aritméticas, estructuras de selección, programación modular y representación de imágenes.

#Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos relacionados a estructuras de repetición y arreglos bi-dimensionales.

2. conocer las funciones básicas de QImage para manipular los pixeles de las imágenes 

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. haber tomado el [quiz Pre-Lab 8](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6791) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).

#Tecnología de pantalla verde ("Green Screen")

En esta experiencia de laboratorio, los estudiantes estarán expuestos a los conceptos y destrezas básicas de la tecnología de pantalla verde que se usa en boletines informativos, películas, juegos de video y otros. La composición de pantalla verde, o composición cromática, es una técnica que se usa para combinar dos imágenes o cuadros de video [1]. Esta técnica de post-producción  crea efectos especiales al componer  dos imágenes o transmiciones de video sustituyendo el área de un color sólido por otra imagen [2]. La composición cromática se puede hacer con imágenes de objetos sobre fondos de cualquier color que sean uniformes y diferentes a los de la imagen. Los fondos azules y verdes son los que se usan con más frecuencia porque se distinguen con más facilidad de los tonos de la mayoría de los colores de piel humanos.

Para esta experiencia de laboratorio te proveemos un interfase gráfico (GUI) simple que le permite al usuario cargar una imagen con un objeto sobre un fondo de color sólido (preferiblemente azul o verde) y una imagen para sustituir el fondo. Tu tarea es crear e implantar una función que cree una tercera imagen compuesta en la cual, a la imagen del objeto  con el fondo de color sólido se le removerá el color de fondo y el objeto aparecerá sobre la imagen que será el nuevo fondo. La Figura 1 muestra un ejemplo de los resultados esperados.

<div align='center'><img src="http://imgur.com/6eonO0l.png"></div>

**Figura 1.** Ejemplo de los resultados esperados. El objeto de interés es la mano con las gafas.


Con el propósito de ilustrar el procedimiento, llamemos la imagen del objeto con el fondo de color sólido  *imagen A*, y supongamos que el color sólido en el fondo tiene un "RGB" `0x00ff00` (verde puro). Llamemos *imagen B* a la imagen que usaremos para el fondo, un fondo que resulte interesante. Para este ejemplo, supongamos también que los tamaños de ambas imágenes son iguales (mismo ancho y alto).

Para producir la imagen compuesta (*imagen C*), podríamos comenzar copiando toda la *imagen B* que usaremos de fondo a la *imagen C*. Luego, para insertar solo el objeto que nos interesa en la imagen compuesta podemos recorrer la *imagen A* pixel por pixel. Compararíamos el color de cada pixel *p* en la *imagen A* con el color de fondo `0x00ff00`. Si son *similares*, el pixel de la *imagen A* corresponde al color sólido de fondo y dejamos el pixel de la *imagen C* como está (el fondo nuevo). Si el color de *p* no es *similar* a `0x00ff00`, modificamos el pixel correspondiente en la *imagen C*, copiando el color del pixel del objeto a la imagen compuesta. Esto se ilustra en la Figura 2.

<div align='center'><img src="http://i.imgur.com/fDmWQrp.png"></div>

**Figura 2.** Ilustración de cómo el algoritmo decide cuáles pixeles de la *imagen A* incluir.


## Midiendo la similaridad de los colores de los pixeles

Observa la Figura 3 abajo. Aunque el fondo en la *imagen A* parece uniforme, realmente incluye pixeles de diferentes colores (aunque parecidos).

<div align='center'><img src="http://i.imgur.com/Fg9Gbly.png"></div>

**Figura 3.** Lo que puede parecer un color *sólido*, realmente no lo es. 


Por esto, en lugar de solo considerar como parte del fondo sólido los pixeles cuyo color es **exactamente** `0x00FF00`, medimos la *distancia* del valor del color del pixel al valor del color *puro*. Una distancia pequeña significa que el color es *casi* verde puro. La ecuación para la *distancia* es:


$$distance = \sqrt{(P_R-S_R)^2+(P_G-S_G)^2+(P_B-S_B)^2},$$

donde $P_R, P_G, P_B$ son los valores de los componentes rojo, verde y azul del pixel bajo consideración, y $S_R, S_G, S_B$ son los valores de los componentes rojo, verde y azul del fondo sólido. En nuestro ejemplo, $S_R=S_B=0$ y $S_G=255$.

#Biblioteca

Para esta experiencia de laboratorio, los estudiantes deben conocer las funciones básicas de QImage para manipular los pixeles de las imágenes 


* `width()`      // captura el ancho de la imagen
* `height()`      // captura la altura de la imagen
* `pixel(i, j)`       // captura el QRgb del pixel en la posición $(i,j)$
* `setPixel(i,j, pixel)`   // modifica el valor del pixel en la posición $(i, j)$ al valor pixel QRgb,

y las funciones para extraer los tonos de los colores rojo, verde y azul de un pixel QRgb 

* `qRed(pixel)`   // captura el tono del color rojo del pixel
* `qGreen(pixel)` // captura el tono del color verde del pixel
* `qBlue(pixel)`  // captura el tono del color azul del pixel
* `qRgb(red, green, blue)` // construye un pixel QRgb 


#Sesión de laboratorio

En el laboratorio de hoy, comenzando con una imagen con un objeto de interés sobre un fondo de color sólido y una imagen para utilizar como fondo, definirás e implantarás una función que cree una tercera imagen compuesta en la cual, a la imagen del objeto de interés se le removerá el color de fondo y aparecerá sobre la imagen para el fondo.



##**Instrucciones:**

1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab08-repetitions02.git` para descargar la carpeta `Lab08-Repetitions02` a tu computadora. La carpeta `GreenScreen` contiene el esqueleto de una aplicación para utilizar la técnica de composición cromática.

2.  Marca doble "click" en el archivo `GreenScreenLab.pro` para cargar este proyecto a Qt y corre el programa.


###**Ejercicio 1**

1. Marca el botón para cargar una imagen del objeto sobre fondo sólido, luego marca el botón para seleccionar la imagen para el fondo. El directorio con los archivos fuente contiene una carpeta llamada `images`  que contiene imágenes de muestra.

2. Tu primera tarea es completar la función `MergeImages` en el archivo `Filter.cpp`. La función `MergeImages` se invoca cuando el usuario marca el botón `Merge Images` y cuando se desliza la barra. La función `MergeImages` recibe las referencias a la imagen con objeto de interés y fondo sólido, la imagen para el fondo y la imagen compuesta, un valor umbral, las coordenadas `(x,y)` de un pixel de la imagen del objeto sobre fondo sólido, y las coordenadas `(offset_x, offset_y)` de la imagen compuesta.  

* `objectImage`: referencia a la imagen del objeto de interés y fondo sólido
* `backgroundImage`: referencia a la imagen para el fondo
* `mergedImage`: referencia a la imagen compuesta
* `threshold`: valor umbral usado para comparar las distancias entre el valor del color del pixel de la imagen con el objeto sobre fondo sólido. En el código que se provee, el valor del umbral se lee del valor de la barra deslizable.
* `ghost`: valor Booleano utilizado para aplicar el filtro "fantasma" a los pixeles. 
* `(x, y)`: coordenadas de un pixel de la imagen del objeto sobre fondo sólido. El valor por defecto es `(0,0)`. 
* `(offset_x, offset_y)`: coordenadas de la imagen compuesta en donde la esquina superior izquierda de la imagen del objeto sobre fondo sólido será insertada. El valor por defecto es `(0,0)`. 

Para este ejercicio puedes ignorar el filtro "fantasma" y las coordenadas `(offset_x, offset_y)`, y solo componer la imagen con el objeto en la imagen de fondo, comenzando en la posición `(0,0)`.

**Algoritmo**

1. El color sólido será el color del pixel en la posición `(x,y)` en la imagen del objeto sobre fondo sólido.
2. Para todas las posiciones `(i,j)`, lee  el valor del color del pixel en la posición `(i,j)` de la imagen con el objeto. Computa la distancia entre el color de la imagen con el objeto y el valor del color sólido. Si la distancia entre el color sólido y el color del pixel de la imagen es mayor que la recibida para el valor umbral, cambia el valor del color del pixel en la posición `(i,j)`de la imagen de fondo al valor del color de la imagen con el objeto.
 
Prueba tu implantación cargando imágenes de objetos e imágenes para el fondo y verificando la imagen compuesta.


###**Ejercicio 2**

En este ejercicio modificarás el Ejercicio 1 para aplicar el filtro fantasma a cada uno de los pixeles que se compondrán sobre la imagen de fondo en el caso de que la variable `ghost` sea cierta. El filtro fantasma creará el efecto de que el objeto en la imagen compuesta se verá como un "fantasma" sobre la imagen de fondo, como en la Figura 4.

<div align='center'><img src="http://imgur.com/SYWmFZO.png"></div>

**Figura 4.** En este ejemplo, el perro en la imagen con el fondo sólido se compone sobre la imagen de fondo utilizando el filtro fantasma.

El efecto fantasma se consigue promediando el valor del color del pixel del fondo con el valor del color del pixel correspondiente del objeto, en lugar de solo reemplazar el valor del color del pixel del fondo por el del objeto. Calculamos el promedio de cada uno de los componentes (rojo, verde y azul)

$$N_R=\frac{S_R+B_R}{2}$$
$$N_G=\frac{S_G+B_G}{2}$$
$$N_B=\frac{S_B+B_B}{2},$$


en donde $N_R, N_G, N_B$ son los componentes rojo, verde y azul del nuevo pixel fantasma,  $S_R, S_G, S_B$ son los componentes de la imagen del objeto, y $B_R, B_G, B_B$ son los componentes de la imagen de fondo.


###**Ejercicio 3**

El "widget" que despliega el fondo fue programado para que detecte la posición marcada por el usuario. En este ejercicio programarás la función `MergeImages` para que el objeto sea desplegado en la posición marcada por el usuario en la imagen de fondo, en lugar de ser desplegado en la esquina superior izquierda. Las Figuras 5 y 6 muestran el efecto. Nota los valores de `Selected Coord` bajo la imagen del medio.

<div align='center'><img src="http://imgur.com/CWFUHeY.png"></div>


**Figura 5.** En este ejemplo, la imagen del fondo no ha sido marcada y `Selected Coord` tiene `(0,0)` que es su valor por defecto. El perro se inserta en la imagen compuesta con su esquina superior izquierda en el lugar `(0,0)`.

<div align='center'><img src="http://imgur.com/8cBtqAR.png"></div>


**Figura 6.** En este ejemplo, la imagen del fondo fue marcada en las coordenadas `(827,593)`. La imagen del perro se inserta en la imagen compuesta con su esquina superior izquierda en la posición `(827,593)`.


Tu tarea en este ejercicio es la misma que en el Ejercicio 1, pero esta vez debes ajustar la imagen del objeto dentro de la composición con las cantidades especificadas en los parámetros  `x_offset` y `y_offset`. Recuerda tomar en consideración los límites de la imagen compuesta cuando insertes el objeto; el usuario pudiera especificar unos parámetros que se salgan de los límites y el objeto se cortará, como sucede en la Figura 7.

<div align='center'><img src="http://imgur.com/TAqNBTG.png"></div>

**Figura 7.** En este ejemplo, el usuario especificó un valor demasiado grande para `x_offset` y solo la mitad del perro salió en la imagen compuesta.

Valida tu implantación seleccionando varios valores para el ajuste de "offset" y observando el efecto que tienen en la imagen compuesta. Asegúrate de tratar casos en los que tus valores para `x_offset` y `y_offset` ocasionarían que la imagen fuera cortada como ocurrió en la imagen compuesta de la Figura 7.




### Entregas 

Copia la versión final de la función `MergeImages` en la sección correspondiente de la página de [Entregas del Lab 8](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6795). También entrega un archivo con la función en [Entrega archivo Lab 8](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=6796). Recuerda incluir tu nombre, comentar adecuadamente la función y usar buenas prácticas de indentación y selección de  nombres para las variables.



### Referencias

[1] http://en.wikipedia.org/wiki/Green_screen_(disambiguation)

[2] http://en.wikipedia.org/wiki/Chroma_key
